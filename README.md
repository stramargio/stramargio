<h1>Hi! I'm <strong>Andrea Margiovanni</strong></h1>
<p>SMC, DPO, Digital Consultant, Augmented Reality Specialist, Business Analyst, Web & Security.</p>
<p>::ffff:7f00:1 -> 🇮🇹 Italy.</p>
<p>🫂❤️ -> a beautiful wife and a fluffy cat.</p>
<br />
<p>You can find me on <a rel="me" href="https://words.margio.de/@andrea">words.margio.de/@andrea</a>, too.</p>

👋🏻
